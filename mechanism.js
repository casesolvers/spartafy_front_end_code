/*
*
*Basic usage:
*   - addCaseIcon(iconName): adds a new case icon, with the bootstrap material icon defined as parameter
*   - removeCaseIcon(index): removes the case icon with the given index (starts from 0)
*   - modifyCaseNotiNumber(index, newNotiNum): sets the indexth case icon's notifications number to newNotiNum
*   - openViewer(HTML): opens the viewer with the html file provided as parameter
*
*/
//Scripts here act as helper functions, the really useful functions are below
    //these are variables used for preventSearch
var actualPrevSearchClass = 0;
var unsearchableSheet = (function() {
	var style = document.createElement("style");
	style.appendChild(document.createTextNode(""));
	document.head.appendChild(style);
	return style.sheet;
})();
NodeList.prototype.forEach = Array.prototype.forEach;
HTMLCollection.prototype.forEach = Array.prototype.forEach;
function makeUnsearchable(element,sheet){
    if(element.children.length == 0){
        if(element.innerText != ""){
            element.innerText.replace('"', '\"');
            sheet.insertRule('.prevSearch'+actualPrevSearchClass+':before { content: "'+element.innerText+'"; }');
            element.classList.add("prevSearch"+actualPrevSearchClass);
            element.innerText = "";
            actualPrevSearchClass++;
        }
    }
    else{
        element.children.forEach(function(entry) {
                makeUnsearchable(entry, sheet);
        });
    }
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function selectCaseIcon(caseIndex){
    var icon = document.getElementsByClassName("caseIcon")[caseIndex];
    return icon;
}
function getIconIndex(iconName){
    var icons = document.getElementsByClassName("caseIcon");
    for(i=0; i<icons.length; i++){
        if(icons[i].children[0].innerText == iconName) return i;
    }
    return -1;
}
function selectChapterList(caseIndex){
    var list = document.getElementsByClassName("list-group")[caseIndex];
    return list;
}
function removeChapterList(index){
    var toRemove = selectChapterList(index);
    toRemove.parentElement.removeChild(toRemove);
}
function hideNotiBadge(caseIndex){
    var icon = selectCaseIcon(caseIndex);
    icon.children[1].style.visibility = "hidden";
}
function showNotiBadge(caseIndex){
    var icon = selectCaseIcon(caseIndex);
    icon.children[1].style.visibility = "visible";
}
function closeActiveViewer(){
    var viewer = document.getElementsByClassName("chapterView")[0]
    if(viewer != undefined) viewer.parentElement.removeChild(viewer);
}
function fillViewer(HTML, iconIndex, title){
    document.getElementsByClassName("cVheader")[0].children[1].children[1].innerText = title;
    document.getElementsByClassName("cVheader")[0].children[1].children[0].innerText = document.getElementsByClassName("caseIcon")[iconIndex].innerText;
    $("#cVbody").load(HTML);
    var today = new Date();
    currentDate = today.getFullYear() + "." + String(today.getMonth() + 1).padStart(2, '0') + "." + String(today.getDate()).padStart(2, '0') + ".";
    document.getElementsByClassName("cVfooter")[0].innerText = currentDate;
}
function openEmptyViewer(){
    var container = document.getElementsByClassName("col-11 d-flex justify-content-center")[0];
        var chapterView = document.createElement("DIV");
        chapterView.classList.add("chapterView");
            var header = document.createElement("DIV");
            header.classList.add("cVheader");
            header.classList.add("d-flex");
            header.classList.add("align-items-center");
            header.classList.add("justify-content-between");
            header.innerHTML = '<div class="p-1 emptyW"></div><div class="d-flex"><i class="p-1 material-icons">view_headline</i><div class="p-1">Title</div></div><div class="closeButtonContainer"><button type="button" name="close" class="cVclose" onClick="closeActiveViewer()"><i class="p-1 material-icons">close</i></button></div>';
            var cVbody = document.createElement("DIV");
            cVbody.classList.add("cVbody");
            cVbody.id = "cVbody";
            var cVfooter = document.createElement("DIV");
            cVfooter.classList.add("cVfooter");
            chapterView.appendChild(header);
            chapterView.appendChild(cVbody);
            chapterView.appendChild(cVfooter);
    container.appendChild(chapterView);
}
function showPopover(icon){
    document.getElementsByClassName("chapterSelectorPopover")[0].style.visibility = "visible";
    document.getElementsByClassName("chapterSelectorPopover")[0].setAttribute('onClick', 'hidePopover()');
    icon.style["z-index"] = 11;
    //itt rejtsük el az összes olyan listát, ami nem az aktuális ikonhoz tartozik
    myIndex = getIconIndex(icon.children[0].innerText);
    for(i=0; i<document.getElementsByClassName("caseIcon").length;i++){
        document.getElementsByClassName("list-group")[i].style.visibility = "hidden";
    }
    document.getElementsByClassName("list-group")[myIndex].style.visibility = "visible";
}
function hidePopover(){
    document.getElementsByClassName("chapterSelectorPopover")[0].style.visibility = "hidden";
    var icons = document.getElementsByClassName("caseIcon");
    for(i=0; i<icons.length; i++){
        icons[i].style["z-index"] = 9;
        document.getElementsByClassName("list-group")[i].style.visibility = "hidden";
    }
}
function getPos(el) {
    // yay readability
    for (var lx=0, ly=0;
         el != null;
         lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
    return {x: lx,y: ly};
}
function drawNewList(iconIndex){
    var listContainer = document.getElementsByClassName("ChapterList")[0];
    var newChild = document.createElement("DIV");
    newChild.classList.add("list-group");
    listContainer.appendChild(newChild);
    positionPopover(iconIndex, newChild);
}
function positionPopover(iconIndex, listGroup){
    iconPos = getPos(selectCaseIcon(iconIndex));
    //setting the y coordinate
    iconCenter = iconPos.y + selectCaseIcon(iconIndex).offsetHeight/2;
    listHeight = listGroup.offsetHeight;
    divTop = iconCenter - listHeight/2;
    listGroup.style.position = "fixed";
    topValue = divTop + "px";
    listGroup.style.top = topValue;
    //setting the x coordinate
    iconX = iconPos.x + selectCaseIcon(iconIndex).offsetWidth/2;
    divLeft = iconX + selectCaseIcon(iconIndex).offsetWidth;
    leftValue = divLeft + "px";
    listGroup.style.left = leftValue;
}
//===================================================================================================================================================
//These functions are the ones we really have to use
function modifyCaseNotiNumber(caseIndex, newNotiNum){
    var icon = selectCaseIcon(caseIndex);
    var beforeModify = icon.children[1].innerText;
    if(beforeModify<1){
        showNotiBadge(caseIndex);
    }
    if(newNotiNum < 1) hideNotiBadge(caseIndex);
    icon.children[1].innerText = newNotiNum;
}
function removeCaseIcon(caseIndex){
    var icon = selectCaseIcon(caseIndex);
    icon.parentElement.removeChild(icon);
    removeChapterList(caseIndex);
}
function addCaseIcon(iconName){
    var container = document.getElementsByClassName("col-1 flex-column")[0];
    var newChild = document.createElement("DIV");
    newChild.classList.add("caseIcon");
    newChild.setAttribute('onClick', 'javascript:iconClicked(this)');
    newChild.innerHTML = '<i class="material-icons">'+iconName+'</i><span class="buttonBadge">0</span>';
    container.appendChild(newChild);
    newChild.children[1].style.visibility = "hidden";
    var myIndex;
    for(i=0; i<newChild.parentElement.children.length; i++){
        if(newChild.parentElement.children[i] == newChild) myIndex=i;
    }
    drawNewList(myIndex);
}
async function openViewer(HTML, iconIndex, title){
    closeActiveViewer();
    openEmptyViewer();
    fillViewer(HTML, iconIndex, title);
    await sleep(1000);
    makeUnsearchable(document.getElementsByClassName("cVbody")[0], unsearchableSheet);
    actualPrevSearchClass = 0;
}
function iconClicked(elem){
    var myIndex;
    for(i=0; i<elem.parentElement.children.length; i++){
        if(elem.parentElement.children[i] == elem) myIndex=i;
    }
    var thisIcon = selectCaseIcon(myIndex);
    modifyCaseNotiNumber(myIndex, 0);
    showPopover(thisIcon);
}

function addCaseChapter(iconIndex, title, html){
    var container = selectChapterList(iconIndex);
    //console.log(container);
    var newChild = document.createElement("A");
    newChild.setAttribute('href', '#');
    newChild.classList.add("list-group-item");
    newChild.classList.add("d-flex");
    newChild.classList.add("justify-content-between");
    newChild.classList.add("align-items-center");
    newChild.setAttribute('onClick', 'javascript:openViewer("'+html+'", '+iconIndex+', "'+title+'")');
    newChild.innerHTML = title + '<i class="indicator"></i>';
    container.appendChild(newChild);
    positionPopover(iconIndex, container);
}
function resetLeftSide(){
    var length = document.getElementsByClassName("caseIcon").length;
    for(i=0; i<length; i++){
        removeCaseIcon(0);
    }
    //closeActiveViewer();
}
//===================================================================================================================================================
//I hope this is the only function to call from the right(Q) side
function handleNewQuestion(qId, debug=false){
    //ettől a ponttól kezeljük a bal oldalt
    if(debug==true) console.log("A függvénybe léptnk");
    currentChapters = [];
    chapterDivs = document.getElementsByClassName("list-group");
    if(debug==true) console.log("Megvan a chapterDivs");
    if(debug==true) console.log(chapterDivs);
    for(i=0;i<chapterDivs.length;i++){
        for(j=0; j<chapterDivs[i].children.length; j++){
            var html = chapterDivs[i].children[j].getAttribute('onClick');
            if(html) html = html.substring(23).split("\"")[0];
            currentChapters.push([document.getElementsByClassName("caseIcon")[i].children[0].innerText, chapterDivs[i].children[j].childNodes[0].nodeValue, html]);
        }
    }
    if(debug==true) console.log("Végigmentünk a chapterDivs-en");
    $.ajax({
        url: "ajaxHelper.php?getChapters&qid="+qId,
        success: function(data){
            if(debug==true) console.log("Lefutott az ajax:");
            if(debug==true) console.log("../components/ajax/leftSideAjaxHelper.php?getChapters&qid="+qId);
            var newData = data.split("<br>");
            newData.splice(-1,1);
            var newChapters = [];
            for(i=0;i<newData.length; i++){
                newData[i] = newData[i].split('¤');
                newChapters.push([newData[i][0],newData[i][1],newData[i][2]]);
            }
            if(debug==true) console.log("Most jön a resetLeftside");
            resetLeftSide();
            if(debug==true) console.log("Resetleftside lefutott.");
            var icons = [];
            for(i=0;i<newChapters.length;i++){
                if (!icons.includes(newChapters[i][0])) icons.push(newChapters[i][0]);
            }
            icons.forEach(function(entry) {
                addCaseIcon(entry);
                if(debug==true) console.log("Most hozzáadjuk az ikont:");
                if(debug==true) console.log(entry);
            });
            newChapters.forEach(function(entry){
                addCaseChapter(getIconIndex(entry[0]), entry[1], entry[2]);
                if(debug==true) console.log("Most hozzáadjuk a chaptert");
                if(debug==true) console.log(entry);
            });
            var realNews = [];
            for(i=0;i<newChapters.length;i++){
                isNew = true;
                for(j=0;j<currentChapters.length;j++){
                    if(currentChapters[j][0] == newChapters[i][0] && currentChapters[j][1] == newChapters[i][1] && currentChapters[j][2] == newChapters[i][2]) isNew=false;
                }
                if(isNew) realNews.push(newChapters[i]);
            }
            if(debug==true) console.log("Már megvannak a tényleg újak");
            if(debug==true) console.log(realNews);
            /*console.log("current chapters:");
            console.log(currentChapters);
            console.log("new chapters:");
            console.log(newChapters);
            console.log("real new ones:");
            console.log(realNews);*/
            //szaladjunk végig a tényleg újakon, és adjunk egy noti badge-et az ikonoknak
            var newNotis = [];
            realNews.forEach(function(entry){
                newNotis.push([entry[0]]);
            });
            var counts = [];
            newNotis.forEach(function(x){ 
                counts[x] = (counts[x] || 0) + 1;
            });
            icons.forEach(function(entry){
                if(counts[entry] == undefined) counts[entry] = 0;
                modifyCaseNotiNumber(getIconIndex(entry), counts[entry]);
            });
            if(debug==true) console.log("Hozzáadtuk a notikat is");

            //TODO: ha a viewerben lévő cucc nincs a newChapters között, akkor zátjuk be a viewert

        },
        error: function (request, status, error) {
            console.log(request.responseText);
        }
    });
    //ettől a ponttól kezeljük a progress barokat
    //a qId adott
        //DONE: a kisebb location-ű progress barok maxolódjanak ki, a label maradjon
        //DONE: a nagyobb location-ű progress barok maradjanak minimumon, a label maradjon
        //DONE: a current location-ű progress labelnek legyen classa a "currentModuleProgressLabel"
        //a current location-ű progress bar width-je legyen a kérdésnek megfelelő
        //DONE: a current location-ű progress labelben megfelelő szöveg legyen (n/max)
    $.ajax({
        url: "ajaxHelper.php?handleBars&qid="+qId,
        success: function(data){
            datas = data.split('¤'); //0:currentLocation | 1:qCount | 2:qNo
            bars = document.getElementsByClassName("progress-bar");
            for(i=0;i<bars.length;i++){
                if(parseInt(bars[i].id.substring(4)) < datas[0]){ //ha actualnál kisebb a location
                    bars[i].style.width = "100%";
                }
                if(parseInt(bars[i].id.substring(4)) > datas[0]){ //ha actualnál nagyobb a location
                    bars[i].style.width = "0%";
                }
                if(parseInt(bars[i].id.substring(4)) == datas[0]){ //ha a jelenlegi modulról beszélünk
                    var label = document.getElementById("labelM"+datas[0]);
                    label.classList.add("currentModuleProgressLabel");
                    label.innerText = datas[2] + '/' + datas[1];
                    var bar = document.getElementById("barM"+datas[0]);
                    bar.style.width = Math.ceil((datas[2]/datas[1])*100) + "%";
                }
            }
        },
        error: function (request, status, error) {
            console.log(request.responseText);
        }
    });
}