<?php
    include 'dbConnect.php';
    $link = getDB();
?>
<?php
if(isset($_GET['getChapters']) && isset($_GET['qid'])){
    $qid = mysqli_real_escape_string($link, $_GET['qid']);
    $chapterIndexQ = 'SELECT chapters FROM Question WHERE id='.$qid.';';
    $chapterIds = mysqli_fetch_array(mysqli_query($link, $chapterIndexQ))[0];
    $chapterIds = explode(",",$chapterIds);
    $returnValue = "";

    for($i=0; $i<count($chapterIds); $i++){ //for every chapter
        $chapterQuery = 'SELECT * FROM Chapter WHERE id='.$chapterIds[$i].';';
        $chapters = mysqli_query($link, $chapterQuery);
        while($row = mysqli_fetch_array($chapters)){
            $returnValue .= mysqli_fetch_array(mysqli_query($link, 'SELECT icon FROM Tab WHERE id='.$row['Tab_id'].';'))[0]; // icon of chapter
            $returnValue .= '¤'; //altGr+ű
            $returnValue .= $row['name'];
            $returnValue .= '¤';
            $returnValue .= $row['html'];
            $returnValue .= '<br>';
        }
    }
    echo $returnValue;
}
if(isset($_GET['handleBars']) && isset($_GET['qid'])){
    $qid = mysqli_real_escape_string($link, $_GET['qid']);
    $mid = mysqli_fetch_array(mysqli_query($link, 'SELECT Module_id FROM Question WHERE id='.$qid.';'))[0];

    $currLoc = mysqli_fetch_array(mysqli_query($link, 'SELECT location FROM Module WHERE id='.$mid.';'))[0];
    $qCount = mysqli_fetch_array(mysqli_query($link, 'SELECT COUNT(DISTINCT no) FROM Question WHERE Module_id='.$mid.';'))[0];
    $qNo = mysqli_fetch_array(mysqli_query($link, 'SELECT no FROM Question WHERE id='.$qid.';'))[0];

    $result = $currLoc.'¤'.$qCount.'¤'.$qNo;
    echo $result;
}
?>