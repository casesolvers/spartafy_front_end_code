
function linkCopyAlert(show) {
    width = $(window).width();
    
    height = $(window).scrollTop();
    $(show).css("top", height + 50)
    $(show).show();
    $(show).addClass("show");
    $(show).removeClass("hide");
    $(show).addClass("showAlert");
  
    setTimeout(function () {
        $(show).removeClass("show");
        $(show).addClass("hide");
  
      }, 3000);
    setTimeout(function () {
        $(show).hide();
        $(window).width(width);
  
      }, 3500);
    
  }
function toggle_middle() {
    $(".changelog .boxmiddle").hide();
    $("body").on("click", ".changelog .boxtop", function () {
        $(this).siblings(".boxmiddle").toggle();
        $(this).siblings(".boxbottom").toggle();
        if ($(this).css("border-bottom-left-radius") != "0px" && $(this).css("border-bottom-right-radius") != "0px") {
            $(this).css("border-bottom-left-radius", "0px");
            $(this).css("border-bottom-right-radius", "0px");
        }
        else {
            $(this).css("border-bottom-left-radius", "10px");
            $(this).css("border-bottom-right-radius", "10px");
        }
        if ($(this).find(".toggler").hasClass("fa-chevron-down") == true) {
            $(this).find(".toggler").removeClass("fa-chevron-down");
            $(this).find(".toggler").addClass("fa-chevron-up");
        }
        else {
            $(this).find(".toggler").removeClass("fa-chevron-up");
            $(this).find(".toggler").addClass("fa-chevron-down");
        }
    });

}

function removeLastBorder() {
    $(".changelog").each(function () {
        $(this).find(".col-12").last().css("border-bottom", "none");
    })
}

function addNew() {
    $("body").on("click", ".boxbottom .add", function () {
        getTime();
        changedCheckbox();
        toggleLogs();
        $(".new_form").show();
        $(".add").hide();
        $(".send").show();
        
    });

    $("body").on("click", ".boxbottom .send", function () {
        getTime();
        changedCheckbox();
        toggleLogs();
        addLog();
    });

    $("input[type=text]").on("click", function (e) {
        e.preventDefault();
    })
    function toggleLogs() {
        if ($(".send:visible").length == 0) {
            $(".changelog").each(function () {
                $(this).find(".boxmiddle").hide();
                $(this).find(".boxtop").css("border-bottom-left-radius", "10px");
                $(this).find(".boxtop").css("border-bottom-right-radius", "10px");
                $(this).find(".toggler").removeClass("fa-chevron-up");
                $(this).find(".toggler").addClass("fa-chevron-down");
            })
        }
    }

    function getTime() {
        var d = new Date();

        var month = d.getMonth() + 1;
        var day = d.getDate();
        var hour = d.getHours();
        var minute = d.getMinutes();
        var second = d.getSeconds();
        var time = (hour < 10 ? '0' : '') + hour + ":" + (minute < 10 ? '0' : '') + minute + ":" + (second < 10 ? '0' : '') + second;

        var output = d.getFullYear() + '.' +
            (month < 10 ? '0' : '') + month + '.' +
            (day < 10 ? '0' : '') + day + '. ' +
            time;

        $(".currentdate").html(output);
    }

    function addLog() {
     
            var date = $(".currentdate").html();
            var status = $("input[name=status]:checked").val();
            var type = $("input[name=type]:checked").val();
            /*var platform = "";
            $("input[name=platform]:checked").each(function () {
                if ($(this).val() != "undefined") {
                    platform += $(this).val() + " ";
                }
            })*/

            var note_creator = $("input[name=note_creator]").val();
            var note_am = $("input[name=note_am]").val();
            var note_test = $("input[name=note_test]").val();
            var note_billing = $("input[name=note_billing]").val();
            var note_reports = $("input[name=note_billing]").val();
            var note_other = $("input[name=note_other]").val();
            application=0;            

            $("input[name=platform]").each(function () {
                
                if ($(this).is(':checked')) {
                    application++;
                }
              
            })

            console.log(application);
            if(note_creator != "" || note_am != "" || note_test != "" || note_billing != ""||note_reports != ""||note_other != ""){
                application++;
                
            }


            if (status != undefined && type != undefined && application!=0) {
                var html = ' <div class="changelog p-0 text-center text-lg-left mx-auto mr-auto pb-1"> <div class="shadowbox"> <div class="box p-0 col-12"> <div class="boxtop text-center"> <div class="col-3 text-center">' + date + '</div><div class="col-3 text-center">' + status + '</div><div class="col-3 text-center">' + type + '</div><div class="col-3 text-right"><i class="fas fa-chevron-down pr-2 toggler"></i> </div></div><div class="boxmiddle text-center" style="display: none;">';
                if ($(".toggle_creator").css('display') == 'block') {
                    (note_creator != "" ? 
                    html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">' + note_creator + ' </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Creator <i class="far fa-window-maximize ml-2"></i> </div></div></div>' : html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">No note</div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Creator <i class="far fa-window-maximize ml-2"></i> </div></div></div>');
                }
                if ($(".toggle_am").css('display') == 'block') {
                    (note_am != ""?
                    html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">' + note_am + ' </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Applicant Manager <i class="far fa-window-maximize ml-2"></i> </div></div></div>': html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block"> No note </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Applicant Manager <i class="far fa-window-maximize ml-2"></i> </div></div></div>');
                }
                if ($(".toggle_test").css('display') == 'block') {
                    (note_test != ""?
                    html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">' + note_test + ' </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Spartafy Test <i class="far fa-window-maximize ml-2"></i> </div></div></div>':  html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">No note</div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Spartafy Test <i class="far fa-window-maximize ml-2"></i> </div></div></div>');
                }
                if ($(".toggle_billing").css('display') == 'block') {
                    (note_billing != ""?
                    html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">' + note_billing + ' </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Billing <i class="far fa-window-maximize ml-2"></i> </div></div></div>':html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">No note </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Billing <i class="far fa-window-maximize ml-2"></i> </div></div></div>');
                }
                if ($(".toggle_reports").css('display') == 'block') {
                    (note_reports != ""?
                    html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">' + note_reports + ' </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Reports <i class="far fa-window-maximize ml-2"></i> </div></div></div>': html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">No note </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Reports <i class="far fa-window-maximize ml-2"></i> </div></div></div>');
                }
                if ($(".toggle_other").css('display') == 'block') {
                    (note_other != ""?
                    html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">' + note_other + ' </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Other <i class="far fa-window-maximize ml-2"></i> </div></div></div>': html += ' <div class="d-block col-12 py-2"> <div class="d-inline-block col-8 p-0 text-left"> <div class="smallergrey d-inline-block">No note </div></div><div class="d-inline-block col-4 p-0 text-right"> <div class="bluetext d-inline-block "> Other <i class="far fa-window-maximize ml-2"></i> </div></div></div>');
                }

                html += ' </div></div></div></div>';
                $(".changelog").last().after(html);
                $(".new_form").hide();
                $(".add").show();
                $(".send").hide();
                removeLastBorder();
                $(".toggle_creator, .toggle_am, .toggle_test, .toggle_billing,.toggle_reports,.toggle_other").hide();
                $("#logform")[0].reset();
                
            }
            else {
             
                linkCopyAlert(".alert");
            }

        
    }
    function changedCheckbox() {
        $("input[name=platform]").on("change", function () {
            console.log($(this).val());
            if ($(this).val() == "Creator") {
                $(".toggle_creator").toggle();
            }
            else if ($(this).val() == "Applicant Manager") {
                $(".toggle_am").toggle();
            }
            else if ($(this).val() == "Spartafy Test") {
                $(".toggle_test").toggle();
            }
            else if ($(this).val() == "Billing") {
                $(".toggle_billing").toggle();
            }
            else if ($(this).val() == "Reports") {
                $(".toggle_reports").toggle();
            }
            else if ($(this).val() == "Other") {
                $(".toggle_other").toggle();
            }

        })

    }
}
$(document).ready(function () {
    toggle_middle();
    removeLastBorder();
    addNew();
});

$(document).ready(function () {
    $(window).resize(function () {


    });
});
