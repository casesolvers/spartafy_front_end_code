<?php
require 'dbConnect.php';
$link = getDB();
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="stylesheet" href="css/bundle.css">
  <link rel="stylesheet" href="css/survey.css">
  <link rel="stylesheet" href="css/caseChapterViewers.css">
  <link rel="stylesheet" href="css/chooseOne.css">

  <title>Spartafy main</title>
</head>

<body>
  <div class="container-fluid d-flex h-100 flex-column">
    <div class="row flex-shrink-0 header">
      <div class="col-6">
        <div class="fluid-container h-100">
          <div class="row h-100">
            <div class="col-6 d-flex flex-row align-items-center">
              <div class="pHead">
                <img class="img-fluid" src="images/profilePicture.png" alt="">
              </div>
              <div class="p-2 pName">
                Firstname Lastname
              </div>
            </div>
            <div class="col-6 d-flex flex-row justify-content-end align-items-center">
              <div class="p-2 badgeTitle">
                Your Badges
              </div>
              <div class="p-2 badgePlaceholder"></div>
              <div class="p-2 badgePlaceholder"></div>
              <div class="p-2 badgePlaceholder"></div>
              <div class="p-2 badgePlaceholder"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-6">
        <div class="fluid-container">
          <div class="row">
            <div class="col-6 d-flex flex-row align-items-center">
              <div class="p-2 moduleName">
                Module name
              </div>
            </div>
            <div class="col-6 d-flex flex-row justify-content-end align-items-center">
              <div class="timeLeft">
                00:10:00
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row d-flex flex-fill overflow-auto">
      <div class="col-6 chapterSelectorPopover flex-fill h-100">
        <div class="ChapterList">
        </div>
      </div>
      <div class="col-6 caseChaptersContainer">
        <div class="container-fluid d-flex h-100 flex-column">
          <div class="row d-flex flex-fill align-items-center">
            <div class="col-1 flex-column">
            </div>
            <div class="col-11 d-flex justify-content-center">
              <div class="chapterView">
                <!-- TEST DATA FROM THIS POINT-->
              </div>
            </div>
          </div>
          <div class="row footer align-items-center">
            <div class="col-6">
              <img src="images/ccLogo.png" alt="">
            </div>
            <div class="col-6 casesolvers" align="right">
              <img src="images/casesolversLogo.png" alt="">
            </div>
          </div>
        </div>
      </div>
      <div class="col-6 colPaddingOverride">
        <div class="container-fluid d-flex h-100 flex-column">
          <div class="row d-flex flex-fill align-items-center">
            <div class="col-12 flex-column QAContainer">
              <div class="questionContainer p-3">
                Mi legyen a kérdés?
              </div>
              <div class="answers">

              </div>
            </div>
          </div>
            <div class="row progressBarContainer">
                <?php
                    //a kitöltött sáv hosszát és a felette lévő kis szöveget tudja kezelni a handleNewQuestion JS
                    //ha aktuális modul, akkor a label kapja meg a "currentModuleProgressLabel" classt
                    $sid = 1;
                    $modules = mysqli_query($link, 'SELECT * FROM Module WHERE Survey_id='.$sid.';');
                    while($row = mysqli_fetch_array($modules)):
                ?>
                    <div class='col progressBar'>
                        <label id='labelM<?php echo $row['location'];?>'><?php echo mysqli_fetch_array(mysqli_query($link, 'SELECT COUNT(DISTINCT no) FROM Question WHERE Module_id='.$row['id'].';'))[0];?></label>
                        <div class='progress'>
                            <div class='progress-bar module<?php echo $row['location'];?>' id='barM<?php echo $row['location'];?>' role='progressbar' aria-valuenow='25' aria-valuemin='0' aria-valuemax='100'></div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
          <div class="row nextBtnContainer">
            <button type="button" name="button" data-toggle="modal" data-target="#exampleModalCenter">
            <div class="col-12 btnNext">
              <i>Next</i>
              <i class="material-icons paddingCenter">chevron_right</i>
            </div>
            </button>
          </div>
        </div>


      </div>
    </div>
  </div>
  <!-- START Are you sure? -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="areYouSureContainer d-flex h-100 justify-content-center align-items-center" role="document">
        <div class="areYouSureContent">
          <div class="areYouSureContentContainer h-100 align-items-center">
            <div class="p-2 colPaddingOverride" align="left">
              <div class="areYouSureicon"><i class="material-icons areYouSureiconMiddle">call_split</i></div>
            </div>
            <div class="p-4 areYouSuretitle flex-column">Are you sure?</div>
            <div class="sureSeparator colPaddingOverride"></div>
            <div class="areYouSurecontent d-flex flex-column">
              <button type="button" class="sureBtnPrimary" data-dismiss="modal">Yes</button>
              <button type="button" class="sureBtnSecondery">No</button>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- START Are you sure? -->
  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="mechanism.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>
