-- phpMyAdmin SQL Dump
-- version 4.0.10.16
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2019 at 02:12 PM
-- Server version: 10.2.19-MariaDB
-- PHP Version: 5.2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mc67763_spartafy`
--

-- --------------------------------------------------------

--
-- Table structure for table `Alert`
--

CREATE TABLE IF NOT EXISTS `Alert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_hungarian_ci DEFAULT NULL,
  `image` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `onerror` tinyint(1) NOT NULL,
  `Question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Alert_Question1_idx` (`Question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `Alert`
--

INSERT INTO `Alert` (`id`, `text`, `image`, `onerror`, `Question_id`) VALUES
(1, 'Biztos vagy te ebben?', 'francseszka.jpg', 1, 19);

-- --------------------------------------------------------

--
-- Table structure for table `Answer`
--

CREATE TABLE IF NOT EXISTS `Answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `image` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `correct` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `nextQuestion` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `nextScreen` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `customID` varchar(255) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Question_id` int(11) DEFAULT NULL,
  `QTE_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Answer_Question1_idx` (`Question_id`),
  KEY `fk_Answer_QTE1_idx` (`QTE_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=85 ;

--
-- Dumping data for table `Answer`
--

INSERT INTO `Answer` (`id`, `text`, `image`, `correct`, `nextQuestion`, `nextScreen`, `customID`, `Question_id`, `QTE_id`) VALUES
(1, 'Egy', NULL, 'x', NULL, NULL, NULL, NULL, 1),
(2, 'Kettő', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 'Mindkét válasz helyes', NULL, NULL, NULL, NULL, NULL, NULL, 1),
(4, 'Egy', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(5, 'Kettő', NULL, 'x', NULL, NULL, NULL, 1, NULL),
(6, 'Három', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(7, '0', NULL, NULL, NULL, NULL, NULL, 2, NULL),
(8, '1', NULL, NULL, NULL, NULL, NULL, 2, NULL),
(9, '2', NULL, 'x', NULL, NULL, NULL, 2, NULL),
(10, '4', NULL, NULL, NULL, NULL, NULL, 2, NULL),
(11, NULL, NULL, '2', NULL, NULL, NULL, 3, NULL),
(12, 'Fekete', 'fekete.jpg', 'x', NULL, NULL, NULL, 4, NULL),
(13, 'Sárga', 'sarga.jpg', NULL, NULL, NULL, NULL, 4, NULL),
(14, 'Átlátszó', 'transparent.jpg', NULL, NULL, NULL, NULL, 4, NULL),
(15, 'Lila', NULL, NULL, NULL, NULL, NULL, 5, NULL),
(16, 'Rózsaszín', NULL, 'x', NULL, NULL, NULL, 5, NULL),
(17, 'Magenta', NULL, NULL, NULL, NULL, NULL, 5, NULL),
(18, 'Barna?', NULL, NULL, NULL, NULL, NULL, 6, NULL),
(19, 'Verébszínű', NULL, 'x', NULL, NULL, NULL, 6, NULL),
(20, 'Mi az a veréb?', NULL, NULL, NULL, NULL, NULL, 6, NULL),
(21, 'Ahol az emberek', NULL, 'x', NULL, NULL, NULL, 7, NULL),
(22, 'Sivatagban', NULL, NULL, NULL, NULL, NULL, 7, NULL),
(23, 'Pesten', NULL, 'x', NULL, NULL, NULL, 7, NULL),
(24, 'Nem lakik', NULL, NULL, NULL, NULL, NULL, 7, NULL),
(25, 'Ausztráliában', NULL, 'x', NULL, NULL, NULL, 8, NULL),
(26, 'Állatkertben', NULL, 'x', NULL, NULL, NULL, 8, NULL),
(27, 'Óceánban', NULL, NULL, NULL, NULL, NULL, 8, NULL),
(28, 'Nem létezik ilyen madár', NULL, NULL, NULL, NULL, NULL, 8, NULL),
(29, 'Schimán csak Újbudán', NULL, 'x', NULL, NULL, NULL, 9, NULL),
(30, 'Az egész balkáni régióban elterjedt', NULL, NULL, NULL, NULL, NULL, 9, NULL),
(31, 'Dél-Amerikában', NULL, NULL, NULL, NULL, NULL, 9, NULL),
(32, 'Egy', NULL, NULL, NULL, NULL, NULL, 10, NULL),
(33, 'Kettő', NULL, NULL, NULL, NULL, NULL, 10, NULL),
(34, 'Három', NULL, NULL, NULL, NULL, NULL, 10, NULL),
(35, 'Egyik fenti válasz sem helyes', NULL, 'x', NULL, NULL, NULL, 10, NULL),
(36, 'Kettő', NULL, NULL, NULL, NULL, NULL, 11, NULL),
(37, 'Négy', NULL, NULL, NULL, NULL, NULL, 11, NULL),
(38, 'Hat', NULL, NULL, NULL, NULL, NULL, 11, NULL),
(39, 'Nyolc', NULL, 'x', NULL, NULL, NULL, 11, NULL),
(40, NULL, NULL, '4', NULL, NULL, NULL, 12, NULL),
(41, 'Tojás', 'tojas.jpg', NULL, NULL, NULL, NULL, 13, NULL),
(42, 'Hüvelykujj', 'huvelykujj.jpg', 'x', NULL, NULL, NULL, 13, NULL),
(43, 'Szárny', 'szarny.jpg', NULL, NULL, NULL, NULL, 13, NULL),
(44, 'Retina', NULL, 'x', NULL, NULL, NULL, 14, NULL),
(45, 'Szárnyak', NULL, NULL, NULL, NULL, NULL, 14, NULL),
(46, 'Hüvelykujj', NULL, 'x', NULL, NULL, NULL, 14, NULL),
(47, 'Úszóhártya', NULL, NULL, NULL, NULL, NULL, 14, NULL),
(48, 'Méh', NULL, 'x', NULL, NULL, NULL, 15, NULL),
(49, 'Méhlepény', NULL, NULL, NULL, NULL, NULL, 15, NULL),
(50, 'Emlő', NULL, 'x', NULL, NULL, NULL, 15, NULL),
(51, 'Petefészek', NULL, 'x', NULL, NULL, NULL, 15, NULL),
(52, 'Gyöngyös', NULL, 'x', NULL, NULL, NULL, 16, NULL),
(53, 'Eger', NULL, NULL, NULL, NULL, NULL, 16, NULL),
(54, 'Egerszalók', NULL, NULL, NULL, NULL, NULL, 16, NULL),
(55, 'Cegléd', NULL, NULL, NULL, NULL, NULL, 16, NULL),
(56, 'Bence', NULL, 'Érd', NULL, NULL, NULL, 17, NULL),
(57, 'Franciska', NULL, 'Nyíregyháza', NULL, NULL, NULL, 17, NULL),
(58, 'Zsolti', NULL, 'Gyöngyös', NULL, NULL, NULL, 17, NULL),
(59, 'Juhi', NULL, 'Cegléd', NULL, NULL, NULL, 17, NULL),
(60, 'Zsolti', NULL, '2', NULL, NULL, NULL, 18, NULL),
(61, 'Juhi', NULL, '3', NULL, NULL, NULL, 18, NULL),
(62, 'Bence', NULL, '1', NULL, NULL, NULL, 18, NULL),
(63, 'Franciska', NULL, '4', NULL, NULL, NULL, 18, NULL),
(64, 'camera', NULL, 'x', NULL, NULL, NULL, 19, NULL),
(65, 'anything', NULL, NULL, NULL, NULL, NULL, 19, NULL),
(66, 'shelf', NULL, NULL, NULL, NULL, NULL, 19, NULL),
(67, 'window', NULL, NULL, NULL, NULL, NULL, 19, NULL),
(68, 'table', NULL, NULL, NULL, NULL, NULL, 19, NULL),
(69, 'plushie piggy', NULL, NULL, NULL, NULL, NULL, 19, NULL),
(70, 'Szürke', NULL, 'x', NULL, NULL, NULL, 20, NULL),
(71, 'Barna', NULL, NULL, NULL, NULL, NULL, 20, NULL),
(72, 'Világoskék', NULL, NULL, NULL, NULL, NULL, 20, NULL),
(73, 'Nincs is az irodában szőnyeg', NULL, NULL, NULL, NULL, NULL, 20, NULL),
(74, NULL, NULL, '13', NULL, NULL, NULL, 21, NULL),
(77, '10%', '', '2', NULL, NULL, NULL, 26, NULL),
(78, NULL, NULL, NULL, NULL, NULL, NULL, 29, NULL),
(79, 'Egy', '', 'x', NULL, NULL, 'email', 34, NULL),
(80, 'Kurva jÃ³', '', NULL, NULL, NULL, NULL, 35, NULL),
(81, 'KettÅ‘', '', NULL, NULL, NULL, 'neotun', 34, NULL),
(83, 'Annyira nem jÃ³', '', NULL, NULL, NULL, NULL, 35, NULL),
(84, 'Legjobb ever', '', 'x', NULL, NULL, NULL, 35, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `Chapter`
--

CREATE TABLE IF NOT EXISTS `Chapter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) COLLATE utf8_hungarian_ci NOT NULL,
  `html` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `css` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `Tab_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Chapter_Tab1_idx` (`Tab_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `Chapter`
--

INSERT INTO `Chapter` (`id`, `name`, `html`, `css`, `Tab_id`) VALUES
(1, 'Cseptör1', 'chapter1.html', 'chapter1.css', 1),
(2, 'Chapter two', 'chapter2.html', 'chapter2.css', 1),
(3, 'Chapter 3', 'chapter3.html', 'chapter3.css', 2),
(4, 'Fejezet 4', 'chapter4.html', 'chapter4.css', 2),
(5, 'Ötödik', 'chapter5.html', 'chapter5.css', 2),
(6, 'Meeting memo 1', 'memo1.html', 'memo1.css', 3),
(7, 'unnamed', 'noHTML', 'noCSS', 5),
(8, 'unnamed', 'noHTML', 'noCSS', 4),
(9, 'unnamed', 'noHTML', 'noCSS', 6),
(10, 'unnamed', 'noHTML', 'noCSS', 5),
(11, 'name', 'No HTML set.html', 'No CSS set', 7);

-- --------------------------------------------------------

--
-- Table structure for table `eDiagram`
--

CREATE TABLE IF NOT EXISTS `eDiagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `type` varchar(31) COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eDiagram_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eDiagram`
--

INSERT INTO `eDiagram` (`id`, `location`, `title`, `type`, `Extras_id`) VALUES
(1, 3, 'Diagram Time!', 'AnswerSpeed', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eImage`
--

CREATE TABLE IF NOT EXISTS `eImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `link` varchar(1023) COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eImage_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eImage`
--

INSERT INTO `eImage` (`id`, `location`, `title`, `link`, `Extras_id`) VALUES
(1, 1, 'Kép címe', 'eImage1.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `eInput`
--

CREATE TABLE IF NOT EXISTS `eInput` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `customID` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eInput_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `eInput`
--

INSERT INTO `eInput` (`id`, `location`, `title`, `customID`, `Extras_id`) VALUES
(1, 2, 'Pls enter ur company', 'company', 4),
(2, 3, 'F. Name:', 'first name', 4);

-- --------------------------------------------------------

--
-- Table structure for table `eLogin`
--

CREATE TABLE IF NOT EXISTS `eLogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eLogin_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eLogin`
--

INSERT INTO `eLogin` (`id`, `location`, `title`, `type`, `Extras_id`) VALUES
(1, 1, 'Itt loginolj be!', 'cs', 1);

-- --------------------------------------------------------

--
-- Table structure for table `eSkillRec`
--

CREATE TABLE IF NOT EXISTS `eSkillRec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eSkillRec_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `eSkillRec`
--

INSERT INTO `eSkillRec` (`id`, `location`, `title`, `Extras_id`) VALUES
(1, 1, '', 4),
(2, 2, 'Results', 6);

-- --------------------------------------------------------

--
-- Table structure for table `eText`
--

CREATE TABLE IF NOT EXISTS `eText` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `text` text COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eText_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `eText`
--

INSERT INTO `eText` (`id`, `location`, `title`, `text`, `Extras_id`) VALUES
(1, 2, 'SzövegTitle', 'Ez itt egy szöveg! Yaaaaay!', 1),
(2, 1, 'wrb', 'rvrbrebbbwvwbrberbbwb', 5),
(3, 1, 'Text', 'wgongoiw4nw onio nogn4oign ion go3ng on4oin 3o4hn oi4nh3 4io n4ohin3 ogn4 on43 nh34nh3443nhi4nhign43n34', 6),
(4, 1, 'Welcome', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 7);

-- --------------------------------------------------------

--
-- Table structure for table `eVideo`
--

CREATE TABLE IF NOT EXISTS `eVideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `link` varchar(127) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_eVideo_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `eVideo`
--

INSERT INTO `eVideo` (`id`, `location`, `link`, `title`, `Extras_id`) VALUES
(1, 2, 'This is a video:', 'dQw4w9WgXcQ', 3);

-- --------------------------------------------------------

--
-- Table structure for table `Extras`
--

CREATE TABLE IF NOT EXISTS `Extras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `isQTE` tinyint(1) NOT NULL,
  `background` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `customHTML` varchar(255) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Survey_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Extras_survey1_idx` (`Survey_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Extras`
--

INSERT INTO `Extras` (`id`, `location`, `isQTE`, `background`, `customHTML`, `Survey_id`) VALUES
(1, 1, 0, 'extraBG1.jpg', NULL, 1),
(2, 3, 1, 'QTE1BG.jpg', NULL, 1),
(3, 6, 0, NULL, 'customExtra.html', 1),
(4, 7, 0, NULL, NULL, 1),
(5, 1, 0, NULL, NULL, 4),
(6, 1, 1, NULL, NULL, 5),
(7, 1, 1, NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `Link`
--

CREATE TABLE IF NOT EXISTS `Link` (
  `url` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `starttime` datetime DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `usertries` int(11) DEFAULT NULL,
  `maxfills` int(11) DEFAULT NULL,
  `cslogin` tinyint(1) DEFAULT NULL,
  `customPrefills` text COLLATE utf8_hungarian_ci DEFAULT NULL,
  `password` text COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Survey_id` int(11) NOT NULL,
  PRIMARY KEY (`url`),
  KEY `fk_Link_Survey1_idx` (`Survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `Link`
--

INSERT INTO `Link` (`url`, `name`, `starttime`, `expiration`, `usertries`, `maxfills`, `cslogin`, `customPrefills`, `password`, `Survey_id`) VALUES
('FutureLink', 'Szeptemberi link', '2019-09-01 00:00:01', '2019-12-31 23:59:59', NULL, NULL, 0, 'Freelancer,,', NULL, 1),
('Hello', 'Unnamed link', '0201-01-01 00:00:00', '2020-01-01 00:00:00', 2, NULL, 0, 'Hello', 'z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP DGNKHfuwvY7kxvUdBeoGlODJ6 SfaPg', 6),
('itmutogatasdgrbr', 'Unnamed link', '2019-04-28 00:00:00', '2019-06-08 00:00:00', 5, 20000, 0, '@case-solver.com', 'z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP DGNKHfuwvY7kxvUdBeoGlODJ6 SfaPg', 5),
('tesszt21324536', 'Nemtomlink1', '2019-04-28 00:00:00', '2019-06-08 00:00:00', 3, 500, 0, NULL, 'z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP DGNKHfuwvY7kxvUdBeoGlODJ6 SfaPg', 3),
('TesztLink1', 'Random link tesztelésre', '2019-01-01 00:00:01', '2100-10-10 10:10:10', 5, 2000, 0, 'Case Solvers,,', NULL, 1),
('TesztLinkB', 'Link Bence prefillel', '2019-01-01 00:00:01', '2100-10-10 10:10:10', NULL, NULL, 0, 'Case Solvers,,Bence', NULL, 1),
('thebestspartafyever', 'Unnamed link', '2019-05-28 00:00:00', '2019-06-05 00:00:00', 5, 100000, 0, '@case-solvers.com', 'z4PhNX7vuL3xVChQ1m2AB9Yg5AULVxXcg/SpIdNs6c5H0NE8XYXysP DGNKHfuwvY7kxvUdBeoGlODJ6 SfaPg', 7),
('thebestspartafyever2', 'Unnamed link', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `Module`
--

CREATE TABLE IF NOT EXISTS `Module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` int(11) NOT NULL,
  `name` varchar(63) COLLATE utf8_hungarian_ci NOT NULL,
  `minutes` int(11) DEFAULT NULL,
  `orientation` tinyint(1) DEFAULT NULL,
  `Survey_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Module_survey1_idx` (`Survey_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `Module`
--

INSERT INTO `Module` (`id`, `location`, `name`, `minutes`, `orientation`, `Survey_id`) VALUES
(1, 2, '1. Modul', 10, 1, 1),
(2, 4, 'MÃ¡sodik module', NULL, 0, 1),
(3, 5, 'Harmadik modul', 10, 0, 1),
(5, 1, 'Madarak', 10, NULL, 3),
(6, 2, 'Untitled Module', NULL, NULL, 4),
(7, 2, 'MedÃºzÃ¡s kaland', 12, 0, 5),
(8, 2, 'Modul 1', 10, 0, 7);

-- --------------------------------------------------------

--
-- Table structure for table `QTE`
--

CREATE TABLE IF NOT EXISTS `QTE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `seconds` int(11) NOT NULL,
  `skill` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `secSkill` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_hungarian_ci NOT NULL,
  `media` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Extras_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_QTE_Extras1_idx` (`Extras_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `QTE`
--

INSERT INTO `QTE` (`id`, `text`, `seconds`, `skill`, `secSkill`, `type`, `media`, `Extras_id`) VALUES
(1, 'Hány lába van Deák Bill Gyulának?', 10, 'Calculation', NULL, 'Choose one', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Question`
--

CREATE TABLE IF NOT EXISTS `Question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `skipable` tinyint(1) DEFAULT NULL,
  `difficulty` int(11) DEFAULT NULL,
  `skill` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `secSkill` varchar(63) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `type` varchar(15) COLLATE utf8_hungarian_ci NOT NULL,
  `media` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `rusure` int(11) NOT NULL,
  `chapters` varchar(63) COLLATE utf8_hungarian_ci NOT NULL,
  `Module_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Question_Module1_idx` (`Module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=37 ;

--
-- Dumping data for table `Question`
--

INSERT INTO `Question` (`id`, `no`, `text`, `skipable`, `difficulty`, `skill`, `secSkill`, `type`, `media`, `rusure`, `chapters`, `Module_id`) VALUES
(1, 1, 'Hány lába van a verébnek?', 0, 1, 'Calculation', NULL, 'Choose one', NULL, 1, '1', 1),
(2, 1, 'Hány lába van a gólyának?', 0, 2, 'Calculation', NULL, 'Choose one', NULL, 0, '1,2', 1),
(3, 1, 'Hány lába van a flamingónak?', 0, 3, 'Calculation', NULL, 'Calculate', NULL, 0, '1,2', 1),
(4, 2, 'Milyen színű a feketerigó?', 0, 1, 'Critical thinking', NULL, 'Choose one', NULL, -1, '1', 1),
(5, 2, 'Milyen színű a flamingó?', 0, 2, 'Critical thinking', NULL, 'Choose one', NULL, 0, '1,2', 1),
(6, 2, 'Milyen színű a veréb?', 0, 3, 'Critical thinking', NULL, 'Choose one', NULL, 1, '1,2', 1),
(7, 3, 'Hol lakik a veréb?', 1, 1, 'Geography', NULL, 'Choose more', 'vereb.jpg', -1, '1', 1),
(8, 3, 'Hol lakik a kacagójancsi?', 1, 2, 'Geography', NULL, 'Choose more', 'kacagojancsi.jpg', 0, '1,2', 1),
(9, 3, 'Hol lakik a dolmányos gödlice?', 1, 3, 'Geography', NULL, 'Choose more', 'dolmanyosgodlice.jpg', 1, '1,2', 1),
(10, 1, 'Hány lába van a kutyának?', 0, 1, 'Calculation', NULL, 'Choose one', NULL, 1, '1', 2),
(11, 1, 'Hány lába van két kacsacsőrű emlősnek?', 0, 2, 'Calculation', NULL, 'Choose one', NULL, 0, '1,2', 2),
(12, 1, 'Hány lába van Malac Úrnak?', 0, 3, 'Calculation', NULL, 'Calculate', NULL, -1, '1,2,3', 2),
(13, 2, 'Az alábbiak közül melyikkel rendelkezik a csimpánz?', 0, 1, 'Biology', NULL, 'Choose one', NULL, 0, '1', 2),
(14, 2, 'Az alábbiak közül melyikkel rendelkezik az ember?', 0, 2, 'Biology', NULL, 'Choose more', NULL, 0, '1,2', 2),
(15, 2, 'Az alábbiak közül melyikkel rendelkezik a kacsacsőrű emlős?', 0, 3, 'Biology', NULL, 'Choose more', NULL, 0, '1,2,3', 2),
(16, 1, 'Honnan származik Zsolti?', 0, 1, 'Geography', NULL, 'Choose one', '99WgR24jWcw', -1, '1', 3),
(17, 1, 'Párosítsd össze az embert és a várost!', 0, 2, 'Geography', NULL, 'Match', NULL, 0, '1,3', 3),
(18, 1, 'Rendezd az irodától való távolság (autóval) szerinti növekvő sorrendbe az alábbi emberek származási helyét!', 0, 3, 'Geography', 'Calculation', 'Order', NULL, 0, '1,2,3', 3),
(19, 2, 'No touchie touchie on the ___!', 0, 1, 'Corporate culture', NULL, 'Choose one', NULL, 1, '1', 3),
(20, 2, 'Milyen színű az irodában használt legnagyobb szőnyeg?', 0, 2, 'Corporate culture', NULL, 'Choose one', NULL, 0, '1,2', 3),
(21, 2, 'Hány asztal van az irodában?', 0, 3, 'Corporate culture', 'Calculation', 'Calculate', NULL, 0, '2,3', 3),
(25, 1, '', NULL, 1, NULL, NULL, 'Choose one', NULL, 0, '', 5),
(26, 1, 'HÃ¡ny lÃ¡ba van a rigÃ³nak?', NULL, 2, 'Numerical reasoning', NULL, 'Calculate', NULL, -1, '6', 5),
(27, 1, '', NULL, 3, NULL, NULL, 'Choose one', NULL, 0, '', 5),
(28, 1, '', NULL, 1, NULL, NULL, 'Choose one', NULL, 0, '', 6),
(29, 1, '', NULL, 2, NULL, NULL, 'Choose one', NULL, 0, '', 6),
(30, 1, '', NULL, 3, NULL, NULL, 'Choose one', NULL, 0, '', 6),
(31, 2, '', NULL, 1, NULL, NULL, 'Choose one', NULL, 0, '', 6),
(32, 2, '', NULL, 2, NULL, NULL, 'Choose one', NULL, 0, '', 6),
(33, 2, '', NULL, 3, NULL, NULL, 'Choose one', NULL, 0, '', 6),
(34, 2, 'HÃ¡ny lÃ¡ba van DBGyulÃ¡naK?', 0, 2, 'Analytical thinking', 'Business sense', 'Choose one', NULL, -1, '7, 11', 7),
(35, 1, 'Mennyire lesz jÃ³ a pÃ©nteki Spartafy?', NULL, 2, 'Critical thinking', 'Inspiration', 'Choose one', NULL, -1, '', 8),
(36, 1, '', NULL, 2, NULL, NULL, 'Choose one', NULL, 0, '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `Survey`
--

CREATE TABLE IF NOT EXISTS `Survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) COLLATE utf8_hungarian_ci NOT NULL,
  `client` varchar(63) COLLATE utf8_hungarian_ci NOT NULL,
  `client_logo` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `customIds` varchar(1023) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `lang` varchar(7) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `adaptive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Survey`
--

INSERT INTO `Survey` (`id`, `name`, `client`, `client_logo`, `customIds`, `lang`, `adaptive`) VALUES
(1, 'New Database Test asdasdasdasdasd', 'Case Solvers', NULL, 'company, email, first name', 'HU', 1),
(2, 'O. B First spartafy', 'Case Solvers', NULL, NULL, 'en', 1),
(3, 'Creator Tutorial Test', 'Dani', NULL, 'E-Mail,Name', 'hu', 1),
(4, 'Test2', 'Dani', NULL, 'E-Mail,Name', 'hu', 1),
(5, 'IT Sol meeting mutogatÃ¡s', 'Dani', NULL, 'email,neotun', 'en', 0),
(6, 'Sparta - Zsolti', 'Case Solvers', NULL, 'Name,Email', 'hu', 0),
(7, 'Adri''s Spartafy', 'Adri', NULL, 'email,neptun', 'hu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Tab`
--

CREATE TABLE IF NOT EXISTS `Tab` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `name` varchar(127) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_hungarian_ci DEFAULT NULL,
  `Survey_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Tab_Survey1_idx` (`Survey_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `Tab`
--

INSERT INTO `Tab` (`id`, `icon`, `name`, `description`, `Survey_id`) VALUES
(1, 'email', 'E-Mails', 'You can view your inbox here!', 1),
(2, 'music_note', 'Meeting Memos', 'Your memos from past meetings', 1),
(3, 'Pen', 'Memos', 'The meeting memos you''ve written before.', 3),
(4, 'Email', 'First email', 'Helloka', 6),
(5, 'Email', 'Emails', 'The e-mails you''ve received from your boss.', 5),
(6, 'Email', 'Emails', 'Emails from boss', 7),
(7, 'Pen', 'sedfghj', '', 5);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Alert`
--
ALTER TABLE `Alert`
  ADD CONSTRAINT `fk_Alert_Question1` FOREIGN KEY (`Question_id`) REFERENCES `Question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Answer`
--
ALTER TABLE `Answer`
  ADD CONSTRAINT `fk_Answer_QTE1` FOREIGN KEY (`QTE_id`) REFERENCES `QTE` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Answer_Question1` FOREIGN KEY (`Question_id`) REFERENCES `Question` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Chapter`
--
ALTER TABLE `Chapter`
  ADD CONSTRAINT `fk_Chapter_Tab1` FOREIGN KEY (`Tab_id`) REFERENCES `Tab` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eDiagram`
--
ALTER TABLE `eDiagram`
  ADD CONSTRAINT `fk_eDiagram_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eImage`
--
ALTER TABLE `eImage`
  ADD CONSTRAINT `fk_eImage_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eInput`
--
ALTER TABLE `eInput`
  ADD CONSTRAINT `fk_eInput_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eLogin`
--
ALTER TABLE `eLogin`
  ADD CONSTRAINT `fk_eLogin_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eSkillRec`
--
ALTER TABLE `eSkillRec`
  ADD CONSTRAINT `fk_eSkillRec_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eText`
--
ALTER TABLE `eText`
  ADD CONSTRAINT `fk_eText_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `eVideo`
--
ALTER TABLE `eVideo`
  ADD CONSTRAINT `fk_eVideo_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Extras`
--
ALTER TABLE `Extras`
  ADD CONSTRAINT `fk_Extras_survey1` FOREIGN KEY (`Survey_id`) REFERENCES `Survey` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Link`
--
ALTER TABLE `Link`
  ADD CONSTRAINT `fk_Link_Survey1` FOREIGN KEY (`Survey_id`) REFERENCES `Survey` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Module`
--
ALTER TABLE `Module`
  ADD CONSTRAINT `fk_Module_survey1` FOREIGN KEY (`Survey_id`) REFERENCES `Survey` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `QTE`
--
ALTER TABLE `QTE`
  ADD CONSTRAINT `fk_QTE_Extras1` FOREIGN KEY (`Extras_id`) REFERENCES `Extras` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Question`
--
ALTER TABLE `Question`
  ADD CONSTRAINT `fk_Question_Module1` FOREIGN KEY (`Module_id`) REFERENCES `Module` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Tab`
--
ALTER TABLE `Tab`
  ADD CONSTRAINT `fk_Tab_Survey1` FOREIGN KEY (`Survey_id`) REFERENCES `Survey` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
